# Contributor: Fabio Ribeiro <fabiorphp@gmail.com>
# Maintainer: Andy Postnikov <apostnikov@gmail.com>
pkgname=php81-pecl-redis
_extname=redis
pkgver=6.0.1
_pkgver=${pkgver/_rc/RC}
pkgrel=0
pkgdesc="PHP 8.1 extension for interfacing with Redis - PECL"
url="https://pecl.php.net/package/redis"
arch="all"
license="PHP-3.01"
_phpv=81
_php=php$_phpv
depends="$_php-pecl-igbinary $_php-pecl-msgpack $_php-session"
makedepends="$_php-dev lz4-dev zstd-dev"
source="php-pecl-$_extname-$_pkgver.tgz::https://pecl.php.net/get/$_extname-$_pkgver.tgz"
builddir="$srcdir/$_extname-$_pkgver"
provides="$_php-redis=$pkgver-r$pkgrel" # for backward compatibility
replaces="$_php-redis" # for backward compatibility

install_if="php-$_extname php$_phpv"

build() {
	phpize$_phpv
	./configure --prefix=/usr --with-php-config=php-config$_phpv \
		--enable-redis-igbinary \
		--enable-redis-lz4 --with-liblz4 \
		--enable-redis-lzf \
		--enable-redis-msgpack \
		--enable-redis-zstd
	make
}

check() {
	# Need running redis server
	$_php -d extension=modules/$_extname.so --ri $_extname
}

package() {
	make INSTALL_ROOT="$pkgdir" install

	local _confdir="$pkgdir"/etc/$_php/conf.d
	mkdir -p $_confdir
	echo "extension=$_extname" > $_confdir/20_$_extname.ini
}

sha512sums="
e822c687d3110485329af40f80ba8e931ffd549e01b1d745bb1d01f709bf9e6c89101044372c39f1ab01f7a668fac984664bc6eb38daffbc50e09d4525f7b643  php-pecl-redis-6.0.1.tgz
"
