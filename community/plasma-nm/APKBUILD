# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=plasma-nm
pkgver=5.27.8
pkgrel=1
pkgdesc="Plasma applet written in QML for managing network connections"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="(LGPL-2.1-only OR LGPL-3.0-only) AND LGPL-2.0-or-later"
depends="
	kirigami2
	networkmanager
	"
makedepends="
	extra-cmake-modules
	kcmutils5-dev
	kcompletion5-dev
	kconfigwidgets5-dev
	kcoreaddons5-dev
	kdbusaddons5-dev
	kdeclarative5-dev
	ki18n5-dev
	kiconthemes5-dev
	kio5-dev
	knotifications5-dev
	kservice5-dev
	kwallet5-dev
	kwidgetsaddons5-dev
	kwindowsystem5-dev
	mobile-broadband-provider-info
	modemmanager-qt5-dev
	networkmanager-qt5-dev
	plasma-framework5-dev
	prison5-dev
	qca-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	samurai
	solid5-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/plasma-nm.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-nm-$pkgver.tar.xz"
subpackages="$pkgname-lang $pkgname-mobile"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_MOBILE=ON
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

mobile() {
	pkgdesc="$pkgdesc (mobile KCM's)"
	depends="$depends $pkgname"

	amove usr/share/kpackage
	amove usr/lib/qt5/plugins/kcms
}

sha512sums="
4c74e952dfa1a6d08bd4a2ac6a8e9492fb2d26e282363728cfbe531366b10daeca7b32dd6927b18c83610f33069d5da1fec7e944ea631a346c9c19b0894ea8b5  plasma-nm-5.27.8.tar.xz
"
